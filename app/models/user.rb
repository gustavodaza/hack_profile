class User < ApplicationRecord
  has_many :certifications
  has_many :friends
  has_many :hobbies
  has_one_attached :image
  accepts_nested_attributes_for :certifications, :hobbies

  before_create :image_empty

  private

  def image_empty
    unless image.attached?
      self.image.attach(io:File.open(Rails.root.join("app/assets/images/default.png")), filename: 'default.png' , content_type: "image/png")
    end
  end
end
