class HobbiesController < ApplicationController
  before_action :set_hobby, only: [:show, :edit, :update, :destroy]
  before_action :set_user, only: [:index, :new, :create]


  def index
    @hobbies = Hobby.all
  end


  def show
  end


  def new
    @hobby = Hobby.new
  end


  def edit
  end


  def create
    @hobby = @user.hobbies.build(hobby_params)
      if @hobby.save
      redirect_to user_hobbies_path(@user), notice: 'Hobby was successfully created.'
      else
       render :new
    end
  end


  def update
      if @hobby.update(hobby_params)
      redirect_to user_hobbies_path(@hobby.user.id), notice: 'Hobby was successfully updated.'
      else
       render :edit
    end
  end


  def destroy
    @user = @hobby.user.id
    @hobby.destroy
     redirect_to user_hobbies_path(@user), notice: 'Hobby was successfully destroyed.'
  end

  private

    def set_hobby
      @hobby = Hobby.find(params[:id])
    end

    def set_user
      @user = User.find(params[:user_id])
    end

    def hobby_params
      params.require(:hobby).permit(:name, :description, :preference, :category)
    end
end
