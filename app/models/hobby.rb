class Hobby < ApplicationRecord
  belongs_to :user

  enum category: {Música: 0, Lectoescritura: 1, Aventura: 2, Fitness: 3, Coleccionismo: 4, Entretenimiento: 5, Construcción: 6, Gastronomía: 7, }
end
