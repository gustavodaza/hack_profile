json.firstname user.firstname
json.lastname user.lastname
json.subtitle user.subtitle
json.bio user.bio
json.certifications user.certifications
json.hobbies user.hobbies
if user.image.attached?
  json.image "#{'http://' + 'localhost' + ':3000' + url_for(user.image)}"
else
  json.image "null"
end
json.url user_url(user, format: :json)
