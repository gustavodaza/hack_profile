class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :age, only: [:show], unless: -> {@user.birthdate.nil?}
  before_action :set_certifications, unless: -> {@user.nil?}
  before_action :set_hobbies, unless: -> {@user.nil?}
  before_action :users_empty, only: [:show, :new], if: -> {@user.nil?}


  def index
    @users = User.all
  end


  def show
  end


  def new
    @user = User.new
  end


  def edit
  end


  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_user
      @user = User.find(params[:id])
    end

    def set_hobbies
      @hobbies = @user.hobbies
    end

    def set_certifications
      @certifications = @user.certifications
    end

    def set_friends
      @friends = @user.friends
    end

    def user_params
      params.require(:user).permit(:firstname, :lastname, :subtitle, :email, :username, :bio, :birthdate, :image)
    end

    def age
      @age = Date.today.year - @user.birthdate.year
      @age -= 1 if Date.today < @user.birthdate + @age.years
    end

    def users_empty
      @users = User.all
      redirect_to action: 'show', id: 1 if !@users.blank?
    end

end
