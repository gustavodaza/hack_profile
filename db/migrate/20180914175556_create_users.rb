class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :firstname
      t.string :lastname
      t.string :subtitle
      t.string :email
      t.string :username
      t.text :bio
      t.date :birthdate

      t.timestamps
    end
  end
end
