Rails.application.routes.draw do

  defaults format: :json do
   get '/profile/gustavodaza', to: 'users#show', :id => '1'
  end

  root to: "users#new"
  resources :friends
  resources :users, shallow: true do
    resources :certifications
    resources :hobbies
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
