class CertificationsController < ApplicationController
  before_action :set_certification, only: [:show, :edit, :update, :destroy]
  before_action :set_user, only: [:index, :new, :create]


  def index
    @certifications = Certification.all
  end


  def show
  end


  def new
    @certification = Certification.create
  end


  def edit
  end


  def create
    @certification = @user.certifications.build(certification_params)
     if @certification.save
       redirect_to user_certifications_path(@user), notice: 'Certification was successfullycreated.'
     else
      render :new
    end
  end


  def update
    if @certification.update(certification_params)
      redirect_to user_certifications_path(@certification.user.id), notice: 'Certification wassuccessfully updated.'
    else
       render :edit
    end
  end

  def destroy
    @user = @certification.user.id
    @certification.destroy
    redirect_to user_certifications_path(@user), notice: 'Certification was successfully destroyed.'
  end

  private

    def set_certification
      @certification = Certification.find(params[:id])
    end

    def set_user
      @user = User.find(params[:user_id])
    end


    def certification_params
      params.require(:certification).permit(:name, :url, :document)
    end

end


